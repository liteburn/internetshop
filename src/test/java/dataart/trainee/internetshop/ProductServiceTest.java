package dataart.trainee.internetshop;

import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.models.product.Product;
import dataart.trainee.internetshop.models.product.Type;
import dataart.trainee.internetshop.repository.ProductRepository;
import dataart.trainee.internetshop.repository.TypeRepository;
import dataart.trainee.internetshop.service.ProductService;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.NoSuchProductException;
import dataart.trainee.internetshop.service.impl.ProductServiceImpl;
import dataart.trainee.internetshop.service.mappers.ProductMapper;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.util.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductServiceTest {
    ProductRepository productRepository = Mockito.mock(ProductRepository.class);
    TypeRepository typeRepository = Mockito.mock(TypeRepository.class);
    ProductService productService;
    Product product;
    ProductData productData;
    ProductData productData1;
    ProductMapper productMapper;
    Set<Type> types1;
    Set<Type> types2;
    List<Product> products;

    @BeforeAll
    void initTypeService() {
        productMapper = new ProductMapper(typeRepository);
        productService = new ProductServiceImpl(productRepository, productMapper, typeRepository);

        Type obj = Type.builder()
                .name("Object")
                .build();
        Type type1 = Type.builder()
                .name("Car")
                .build();
        types1 = new HashSet<>(Arrays.asList(type1, obj));
        types2 = new HashSet<>(List.of(obj));

        product = Product
                .builder()
                .name("car")
                .price(100.0)
                .amount(10)
                .description("none")
                .types(types1)
                .build();
        Product product1 = Product
                .builder()
                .name("phone")
                .price(1000.0)
                .amount(10)
                .description("none")
                .types(types2)
                .build();

        products = Arrays.asList(product, product1);
        List<Product> cars = List.of(product);

        Mockito.when(productRepository.findAll()).thenReturn(products);
        Mockito.when(productRepository.findAllByOrderByNameAsc()).thenReturn(products);
        Mockito.when(productRepository.findAllByOrderByNameDesc()).thenReturn(products);
        Mockito.when(productRepository.findAllByNameOrderByNameAsc("car")).thenReturn(cars);
        Mockito.when(productRepository.findAllByNameOrderByNameDesc("car")).thenReturn(cars);
        Mockito.when(productRepository.findAllByNameOrderByNameAsc("car")).thenReturn(cars);
        Mockito.when(productRepository.findById(1)).thenReturn(product);
        Mockito.when(productRepository.findById(2)).thenReturn(product1);
        Mockito.when(typeRepository.findByName("Car")).thenReturn(type1);
        Mockito.when(typeRepository.findByName("Obj")).thenReturn(obj);
    }

    @BeforeEach
    public void init() {
        String name = "car";
        String description = "none";
        double price = 100.0;
        double price2 = 222.0;
        int amount = 10;

        product = Product
                .builder()
                .name(name)
                .price(price)
                .amount(amount)
                .description(description)
                .types(types1)
                .build();

        productData = ProductData
                .builder()
                .name(name)
                .price(price)
                .amount(amount)
                .description(description)
                .build();

        productData1 = ProductData
                .builder()
                .name(name)
                .price(price2)
                .amount(amount)
                .description(description)
                .build();
    }

    @Test
    public void getProductsTest() {
        Assertions.assertEquals(productService.getProducts("car", null, OrderEnum.NAME_DESC).size(), 1);
        Assertions.assertEquals(1, productService.getProducts(null, "Car", OrderEnum.NAME_ASC).size());
        Assertions.assertEquals(productService.getProducts(null, "Object", OrderEnum.DATE_ASC).size(), 2);
        Assertions.assertEquals(productService.getProducts(null, null, null).size(), 2);

    }

    @Test
    public void getProductThatDoesNotExistTest() {
        Exception exception = Assertions.assertThrows(NoSuchProductException.class, () -> productService.getProduct(123));
        String expectedMessage = "No such product founded";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void getProductTest() throws NoSuchProductException {

        ProductData productData = productService.getProduct(1);
        Assertions.assertEquals(product.getAmount(), productData.getAmount());
        Assertions.assertEquals(product.getDescription(), productData.getDescription());
        Assertions.assertEquals(product.getId(), productData.getId());
        Assertions.assertEquals(product.getPrice(), productData.getPrice());
        Assertions.assertEquals(product.getName(), productData.getName());
    }

    @Test
    public void sumUpProductsTest() {
        double expected_price = 0;
        List<ProductData> productsData = new ArrayList<>();
        for (Product product : products) {
            expected_price += product.getPrice() * product.getAmount();
            productsData.add(productMapper.productToProductData(product));
        }
        double actual_price = productService.sumUpProducts(productsData);
        Assertions.assertEquals(expected_price, actual_price);
    }

    @Test
    void addProductTest() {
        productService.addProduct(productData);
    }

    @Test
    void updateProductTest() {
        productService.updateProduct(1, productData);
    }

    @Test
    void deleteProductTest() {
        productService.deleteProduct((long) 1);
    }
}
