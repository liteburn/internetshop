package dataart.trainee.internetshop;

import dataart.trainee.internetshop.models.product.Cart;
import dataart.trainee.internetshop.models.product.Product;
import dataart.trainee.internetshop.models.product.Type;
import dataart.trainee.internetshop.repository.CartRepository;
import dataart.trainee.internetshop.repository.ProductRepository;
import dataart.trainee.internetshop.service.CartService;
import dataart.trainee.internetshop.service.exceptions.InvalidAmountException;
import dataart.trainee.internetshop.service.impl.CartServiceImpl;
import dataart.trainee.internetshop.service.mappers.ProductMapper;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.util.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CartServiceTest {
    ProductRepository productRepository = Mockito.mock(ProductRepository.class);
    CartRepository cartRepository = Mockito.mock(CartRepository.class);
    ProductMapper productMapper = new ProductMapper();
    CartService cartService = new CartServiceImpl(cartRepository, productRepository, productMapper);

    @BeforeAll
    void init() {

        Type obj = Type.builder()
                .name("Object")
                .build();
        Type type1 = Type.builder()
                .name("Car")
                .build();
        Type type2 = Type.builder()
                .name("Toy")
                .build();
        Type type3 = Type.builder()
                .name("Game")
                .build();

        Set<Type> types1 = new HashSet<>(Arrays.asList(type1, obj));
        Set<Type> types2 = new HashSet<>(Arrays.asList(type2, obj));
        Set<Type> types3 = new HashSet<>(Arrays.asList(type3, obj));

        Product product1 = Product.builder()
                .name("car")
                .amount(10)
                .description("Fast")
                .price(1000.24)
                .types(types1)
                .build();
        Product product2 = Product.builder()
                .name("car")
                .amount(12)
                .description("Toy")
                .price(12.4)
                .types(types2)
                .build();
        Product product3 = Product.builder()
                .name("game")
                .amount(10000)
                .description("Game")
                .price(12.4)
                .types(types3)
                .build();
        Long one = Long.parseLong("1");
        Cart cart1 = Cart.builder()
                .productId(one)
                .userId(one)
                .amount(4)
                .build();
        Cart cart2 = Cart.builder()
                .productId(Long.parseLong("2"))
                .userId(one)
                .amount(2)
                .build();
        List<Cart> carts = Arrays.asList(cart1, cart2);
        Mockito.when(productRepository.findById(1)).thenReturn(product1);
        Mockito.when(productRepository.findById(2)).thenReturn(product2);
        Mockito.when(productRepository.findById(3)).thenReturn(product3);
        Mockito.when(cartRepository.findByUserIdAndProductId(one, one)).thenReturn(cart1);
        Mockito.when(cartRepository.findAllByUserId(one)).thenReturn(carts);
        cartService = Mockito.spy(cartService);
        Mockito.doReturn(one).when(cartService).getUserId();
        System.out.println("Done");
    }

    @Test
    void getProductsInCartTest() {
        Assertions.assertEquals(2, cartService.getProducts().size());
    }

    @Test
    void buyCartTest() {
        cartService.buyCart();
    }

    @Test
    void clearCartTest() {
        cartService.clearCart();
    }

    @Test
    void removeProductTest() {
        cartService.removeProduct(1);
        cartService.removeProduct(4);
    }

    @Test
    void addProductTest() throws InvalidAmountException {
        cartService.addProduct(1, 2);
        cartService.addProduct(3, 2);
    }

    @Test
    void addProductThrowsExceptionTest() {
        Assertions.assertThrows(InvalidAmountException.class, () -> cartService.addProduct(1, 200));
    }
}
