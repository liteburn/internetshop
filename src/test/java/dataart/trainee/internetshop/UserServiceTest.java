package dataart.trainee.internetshop;

import dataart.trainee.internetshop.constants.DefaultUser;
import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.models.User;
import dataart.trainee.internetshop.models.enums.UserRole;
import dataart.trainee.internetshop.repository.UserRepository;
import dataart.trainee.internetshop.service.UserService;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.NoSuchCredentialsException;
import dataart.trainee.internetshop.service.exceptions.UserAlreadyExistException;
import dataart.trainee.internetshop.service.exceptions.WrongPasswordException;
import dataart.trainee.internetshop.service.impl.UserServiceImpl;
import dataart.trainee.internetshop.service.mappers.UserMapper;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserServiceTest {
    UserRepository userRepository = Mockito.mock(UserRepository.class);
    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    UserMapper userMapper = new UserMapper(passwordEncoder);
    UserService userService = new UserServiceImpl(userRepository, userMapper);
    List<User> usersAsc;
    List<User> usersDesc;
    User user;

    @BeforeAll
    void mock() throws ParseException {
        User user = DefaultUser.getDefaultUser();
        User user1 = User.builder()
                .name("Arsen")
                .password(passwordEncoder.encode("12345678"))
                .surname("Ilch")
                .date(DefaultUser.getDefaultUser().getDate())
                .email("2@gmail.com")
                .userRole(UserRole.USER)
                .build();
        usersAsc = Arrays.asList(user, user1);
        usersDesc = Arrays.asList(user1, user);

        Mockito.when(userRepository.findAll()).thenReturn(usersAsc);
        Mockito.when(userRepository.findById(Long.parseLong("1"))).thenReturn(user);
        Mockito.when(userRepository.findById(Long.parseLong("2"))).thenReturn(user1);
        Mockito.when(userRepository.findByEmail("2@gmail.com")).thenReturn(user1);
        Mockito.when(userRepository.findByEmail("1@gmail.com")).thenReturn(user);
        Mockito.when(userRepository.findAllByOrderByNameAsc()).thenReturn(usersAsc);
        Mockito.when(userRepository.findAllByOrderByNameDesc()).thenReturn(usersDesc);
        Mockito.when(userRepository.findAllByOrderByDateAsc()).thenReturn(usersAsc);
        Mockito.when(userRepository.findAllByOrderByDateDesc()).thenReturn(usersDesc);
        Mockito.when(userRepository.findAllByOrderByEmailDesc()).thenReturn(usersDesc);
        Mockito.when(userRepository.findAllByOrderByEmailAsc()).thenReturn(usersAsc);


    }

    @BeforeEach
    void init() throws ParseException {
        user = User.builder()
                .name("Arsen")
                .password(passwordEncoder.encode("12345678"))
                .surname("Ilch")
                .date(DefaultUser.getDefaultUser().getDate())
                .email("3@gmail.com")
                .userRole(UserRole.USER)
                .build();
    }

    @Test
    void check() {
        Assertions.assertEquals(userRepository.findAll().size(), usersAsc.size());
    }

    @Test
    void checkIfUserExistReturnFalseTest() {
        Assertions.assertFalse(userService.checkIfUserExist("aaa@gmail.com"));
    }

    @Test
    void checkIfUserExistReturnTrueTest() {
        Assertions.assertTrue(userService.checkIfUserExist("1@gmail.com"));
    }

    @Test
    void updateUserWhenNoSuchUserTest() throws ParseException {
        userService.updateUser(userMapper.userToUserData(user));
    }

    @Test
    void updateUserTest() throws ParseException {
        user.setEmail("2@gmail.com");
        user.setName("newName");
        userService.updateUser(userMapper.userToUserData(user));
    }

    @Test
    void getUserWithEmailTest() throws NoSuchCredentialsException {
        userService.getUser("2@gmail.com");
    }

    @Test
    void getUserWithEmailWhenNotExist() {
        Assertions.assertThrows(NoSuchCredentialsException.class, () -> {
            userService.getUser("3@gmail.com");
        });
    }

    @Test
    void getUserWithIdTest() throws NoSuchCredentialsException {
        userService.getUser(Long.parseLong("1"));
        userService.getUser(Long.parseLong("2"));
    }

    @Test
    void getUserWithIdWhenNotExist() {
        Assertions.assertThrows(NoSuchCredentialsException.class, () -> {
            userService.getUser(Long.parseLong("3"));
        });
    }

    @Test
    void getAllUsersTest() {
        Assertions.assertEquals(2, userService.getAllUsers(null).size());
        Assertions.assertEquals(2, userService.getAllUsers(OrderEnum.DATE_ASC).size());
        Assertions.assertEquals(2, userService.getAllUsers(OrderEnum.NAME_ASC).size());
        Assertions.assertEquals(2, userService.getAllUsers(OrderEnum.NAME_DESC).size());
        Assertions.assertEquals(2, userService.getAllUsers(OrderEnum.EMAIL_ASC).size());
        Assertions.assertEquals(2, userService.getAllUsers(OrderEnum.EMAIL_DESC).size());
    }

    @Test
    void deleteUserTest() {
        userService.deleteUser(2);
    }

    @Test
    void registerTest() throws ParseException, UserAlreadyExistException, WrongPasswordException {
        String name = "Arsen";
        String password = passwordEncoder.encode("12345678");
        String wrongPassword = passwordEncoder.encode("2345678");
        String surname = "Ilch";
        Date date = DefaultUser.getDefaultUser().getDate();
        String email = "3@gmail.com";

        Assertions.assertThrows(UserAlreadyExistException.class, () -> {
            userService.register(userMapper.userToUserData(DefaultUser.getDefaultUser()));
        });

        UserData userDataWithWrongPassword = UserData.builder()
                .name(name)
                .password(password)
                .repeatPassword(wrongPassword)
                .surname(surname)
                .date(date)
                .email(email)
                .build();
        Assertions.assertThrows(WrongPasswordException.class, () -> {
            userService.register(userDataWithWrongPassword);
        });

        UserData userData = UserData.builder()
                .name(name)
                .password(password)
                .repeatPassword(password)
                .surname(surname)
                .date(date)
                .email(email)
                .build();
        userService.register(userData);
    }
}
