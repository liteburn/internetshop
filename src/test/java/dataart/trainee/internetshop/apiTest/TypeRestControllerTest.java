package dataart.trainee.internetshop.apiTest;

import dataart.trainee.internetshop.dto.product.TypeData;
import dataart.trainee.internetshop.repository.UserRepository;
import dataart.trainee.internetshop.restContoller.product.TypeRestController;
import dataart.trainee.internetshop.service.TypeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(controllers = TypeRestController.class)
@WithMockUser(username = "user", roles = "ADMIN")
public class TypeRestControllerTest {
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TypeService typeService;

    private final TypeData typeData = TypeData.builder()
                .name("Type")
                .build();;

    @BeforeEach
    void setData(){
        List<TypeData> lst = new ArrayList<>();
        lst.add(typeData);
        lst.add(TypeData.builder().name("Type2").build());
        Mockito.when(typeService.addType(typeData)).thenReturn(typeData);
        Mockito.when(typeService.getAllTypes()).thenReturn(lst);
    }

    @Test
    void getTypesTest() throws Exception {
        this.mockMvc.perform(get("/api/products/types"))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is(typeData.getName())))
                .andExpect(jsonPath("$[1].name", is("Type2")));
    }

    @Test
    void addTypeTest() throws Exception {
        this.mockMvc.perform(post("/api/products/types")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(ObjectToJson.asJsonString(typeData)))
                .andExpect(jsonPath("$.name", is(typeData.getName())))
                .andExpect(status().isOk());
    }



}
