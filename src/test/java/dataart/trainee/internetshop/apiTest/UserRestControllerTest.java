package dataart.trainee.internetshop.apiTest;


import dataart.trainee.internetshop.constants.DefaultUser;
import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.repository.UserRepository;
import dataart.trainee.internetshop.restContoller.UserRestController;
import dataart.trainee.internetshop.service.UserService;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.NoSuchCredentialsException;
import dataart.trainee.internetshop.service.exceptions.UserAlreadyExistException;
import dataart.trainee.internetshop.service.exceptions.WrongPasswordException;
import dataart.trainee.internetshop.service.mappers.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(controllers = UserRestController.class)
@WithMockUser(username = "user", roles = "ADMIN")
public class UserRestControllerTest {

    @MockBean
    private UserService userService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @Autowired
    private MockMvc mockMvc;
    UserMapper userMapper = new UserMapper();

    UserData userData = userMapper.userToUserData(DefaultUser.getDefaultUser());

    public UserRestControllerTest() throws ParseException {
    }

    @BeforeEach
    void beforeAll() throws ParseException, NoSuchCredentialsException, UserAlreadyExistException, WrongPasswordException {
        userData = userMapper.userToUserData(DefaultUser.getDefaultUser());
        Mockito.when(userService.getAllUsers(OrderEnum.NAME_ASC)).thenReturn(List.of(userData));
        Mockito.when(userService.getUser(1)).thenReturn(userData);
        Mockito.when(userService.updateUser(userData)).thenReturn(userData);
        Mockito.when(userService.register(userData)).thenReturn(userData);
    }

    @Test
    void getUsersTest() throws Exception {
        this.mockMvc.perform(get("/api/users?order=NAME_ASC"))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(jsonPath("$[0].name", is(userData.getName())));
    }

    @Test
    void getUserTest() throws Exception {
        this.mockMvc.perform(get("/api/users/1"))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(jsonPath("$.name", is(userData.getName())));
    }

    @Test
    void registerTest() throws Exception {
        this.mockMvc.perform(post("/api/users")
                        .content(ObjectToJson.asJsonString(userData))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is(userData.getName())));
    }

    @Test
    void  updateUserTest() throws Exception {
        this.mockMvc.perform(put("/api/users/1")
                        .content(ObjectToJson.asJsonString(userData))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(jsonPath("$.name", is(userData.getName())));
    }

    @Test
    void deleteUserTest() throws Exception {
        this.mockMvc.perform(delete("/api/users/1")
                        .content(ObjectToJson.asJsonString(userData))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


}


