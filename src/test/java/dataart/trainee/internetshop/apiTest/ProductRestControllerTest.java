package dataart.trainee.internetshop.apiTest;


import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.repository.UserRepository;
import dataart.trainee.internetshop.restContoller.product.ProductRestController;
import dataart.trainee.internetshop.service.CartService;
import dataart.trainee.internetshop.service.ProductService;
import dataart.trainee.internetshop.service.TypeService;
import dataart.trainee.internetshop.service.UserService;
import dataart.trainee.internetshop.service.exceptions.InvalidAmountException;
import dataart.trainee.internetshop.service.exceptions.NoSuchProductException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(controllers = ProductRestController.class)
@WithMockUser(username = "user", roles = "ADMIN")
public class ProductRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    UserService userService;
    @MockBean
    ProductService productService;
    @MockBean
    TypeService typeService;
    @MockBean
    UserRepository userRepository;
    @MockBean
    private PasswordEncoder passwordEncoder;


    @MockBean
    private CartService cartService;
    ProductData product = ProductData.builder()
            .name("Product")
            .price(100.0)
            .amount(10)
            .description("a")
            .types(List.of("Type1", "Type2"))
            .build();
    @BeforeEach
    void init() throws NoSuchProductException, InvalidAmountException {

        Mockito.when(productService.getProducts(null, null, null))
                .thenReturn(List.of(product));
        Mockito.when(productService.getProduct(1))
                .thenReturn(product);
        Mockito.when(productService.updateProduct(1, product))
                .thenReturn(product);
        Mockito.when(productService.addProduct(product))
                .thenReturn(product);
        Mockito.when(cartService.addProduct(1, 10)).thenReturn(product)
                .thenReturn(product);
        Mockito.when(cartService.getProducts())
                .thenReturn(List.of(product));


       // Mockito.when(cartService.addProduct(1, 1)).thenReturn(product.setAmount(1));
    }

    @Test
    void getProductsTest() throws Exception {
        this.mockMvc.perform(get("/api/products"))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(jsonPath("$[0].name", is(product.getName())));
    }

    @Test
    void getProductTest() throws Exception {
        this.mockMvc.perform(get("/api/products/1"))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(jsonPath("$.name", is(product.getName())));
    }

    @Test
    void addProductTest() throws Exception {
        this.mockMvc.perform(post("/api/products")
                        .content(ObjectToJson.asJsonString(product))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(jsonPath("$.name", is(product.getName())));
    }

    @Test
    void updateProductTest() throws Exception {
        this.mockMvc.perform(put("/api/products/1")
                        .content(ObjectToJson.asJsonString(product))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(jsonPath("$.name", is(product.getName())));
    }

    @Test
    void deleteProductTest() throws Exception {
        this.mockMvc.perform(delete("/api/products?id=1"))
                .andExpect(status().isOk());
    }

    @Test
    void addProductToCartTest() throws Exception {
        this.mockMvc.perform(post("/api/products/cart?id=1&amount=10"))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(jsonPath("$.name", is(product.getName())));
    }

    @Test
    void removeProductFromCartTest() throws Exception {
        this.mockMvc.perform(delete("/api/products/cart?id=1"))
                .andExpect(status().isOk());
    }

    @Test
    void getCartTest() throws Exception {
        this.mockMvc.perform(get("/api/products/cart"))
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype()
                )))
                .andExpect(jsonPath("$[0].name", is(product.getName())));
    }

    @Test
    void buyCartTest() throws Exception {
        this.mockMvc.perform(post("/api/products/buyCart"))
                .andExpect(status().isOk());
    }

    @Test
    void clearCart() throws Exception {
        this.mockMvc.perform(post("/api/products/clearCart"))
                .andExpect(status().isOk());
    }
}
