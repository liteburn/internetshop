package dataart.trainee.internetshop;

import dataart.trainee.internetshop.dto.product.TypeData;
import dataart.trainee.internetshop.models.product.Type;
import dataart.trainee.internetshop.service.mappers.TypeMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TypeMapperTest {
    private final TypeMapper typeMapper = new TypeMapper();

    @Test
    public void TypeToTypeDataTest() {
        String name = "Type";
        Long id = Long.parseLong("1");
        Type type = Type.builder()
                .name(name)
                .id(id)
                .build();
        TypeData typeData = typeMapper.typeToTypeData(type);
        Assertions.assertTrue(compareTypeAndTypeData(type, typeData));
        Assertions.assertEquals(type.getId(), typeData.getId());
    }

    @Test
    public void TypeDataToTypeTest() {
        String name = "Type";
        TypeData typeData = TypeData.builder()
                .name(name)
                .build();
        Type type = typeMapper.typeDataToType(typeData);
        Assertions.assertTrue(compareTypeAndTypeData(type, typeData));
    }

    private boolean compareTypeAndTypeData(Type type, TypeData typeData) {
        return (type.getName().equals(typeData.getName()));
    }
}
