package dataart.trainee.internetshop;

import dataart.trainee.internetshop.constants.DatePatterns;
import dataart.trainee.internetshop.constants.DefaultUser;
import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.models.User;
import dataart.trainee.internetshop.models.enums.UserRole;
import dataart.trainee.internetshop.service.mappers.UserMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class UserMapperTest {

    private final UserMapper userMapper = new UserMapper(new BCryptPasswordEncoder());

    @Test
    public void usersToUsersData() throws ParseException {
        User user = DefaultUser.getDefaultUser();
        user.setId(Long.parseLong("1"));
        List<User> users = new ArrayList<>();
        users.add(user);
        List<UserData> usersData = userMapper.usersToUsersData(users);
        Assertions.assertTrue(compareUserAndUserData(users.get(0), usersData.get(0)));
        Assertions.assertEquals(users.get(0).getId(), usersData.get(0).getId());
        Assertions.assertEquals(users.get(0).getUserRole(), usersData.get(0).getRole());
        Assertions.assertEquals(users.size(), usersData.size());
    }


    @Test
    public void userDataToUserTest() throws ParseException {
        UserData userData = UserData.builder()
                .name("name")
                .surname("surname")
                .email("1@gmail.com")
                .date(new SimpleDateFormat(DatePatterns.DEFAULT_DATE_PATTERN).parse(DefaultUser.DEFAULT_DATE))
                .password("...")
                .build();
        User user = userMapper.userDataToUser(userData);
        Assertions.assertTrue(compareUserAndUserData(user, userData));
    }

    @Test
    public void userToUserDataTest() throws ParseException {
        User user = DefaultUser.getDefaultUser();
        UserData userData = userMapper.userToUserData(user);
        Assertions.assertTrue(compareUserAndUserData(user, userData));
    }

    @Test
    public void updateUserWithUserDataTest() throws ParseException {
        User user = DefaultUser.getDefaultUser();
        UserData userData = UserData.builder()
                .role(UserRole.ADMIN)
                .name("newName")
                .build();
        userMapper.updateUserWithUserData(user, userData);
        Assertions.assertEquals(user.getUserRole(), userData.getRole());
        Assertions.assertEquals(user.getName(), userData.getName());
    }

    private boolean compareUserAndUserData(User user, UserData userData) {
        return (user.getEmail().equals(userData.getEmail())) &&
                (user.getDate().equals(userData.getDate())) &&
                (user.getSurname().equals(userData.getSurname())) &&
                (user.getName().equals(userData.getName()));
    }
}
