package dataart.trainee.internetshop;

import dataart.trainee.internetshop.dto.product.TypeData;
import dataart.trainee.internetshop.models.product.Type;
import dataart.trainee.internetshop.repository.TypeRepository;
import dataart.trainee.internetshop.service.TypeService;
import dataart.trainee.internetshop.service.impl.TypeServiceImpl;
import dataart.trainee.internetshop.service.mappers.TypeMapper;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TypeServiceTest {
    TypeRepository typeRepository = Mockito.mock(TypeRepository.class);
    TypeService typeService;
    TypeMapper typeMapper = new TypeMapper();
    List<Type> types = new ArrayList<>();
    Type type;

    @BeforeAll
    void initTypeService() {
        type = Type.builder().name("Type").build();
        typeService = new TypeServiceImpl(typeRepository, new TypeMapper());
        types.add(type);
        Mockito.when(typeRepository.findAll()).thenReturn(types);
    }

    @Test
    void getAllTypesTest() {

        List<TypeData> types = typeService.getAllTypes();
        Assertions.assertEquals(types.get(0).getName(), types.get(0).getName());
    }

    @Test
    void addType() {
        typeService.addType(typeMapper.typeToTypeData(type));
    }
}
