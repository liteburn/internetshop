package dataart.trainee.internetshop;


import dataart.trainee.internetshop.constants.DefaultUser;
import dataart.trainee.internetshop.controller.WelcomePageController;
import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.repository.UserRepository;
import dataart.trainee.internetshop.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.text.ParseException;

@WebMvcTest(controllers = WelcomePageController.class)
@ActiveProfiles("test")
public class WelcomePageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private PasswordEncoder passwordEncoder;
    private UserData userData;

    @BeforeEach
    void setUp() throws ParseException {
        this.userData = UserData.builder()
                .name("Arsen")
                .password("12345678")
                .repeatPassword("12345678")
                .surname("Ilch")
                .date(DefaultUser.getDefaultUser().getDate())
                .email("3@gmail.com")
                .build();
    }

    @Test
    void getStarterPage() throws Exception {
        Mockito.when(userService.getUser("3@gmail.com")).thenReturn(userData);
        this.mockMvc.perform(get("/"))
                .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk());
    }
}
