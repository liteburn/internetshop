package dataart.trainee.internetshop;


import dataart.trainee.internetshop.constants.DefaultUser;
import dataart.trainee.internetshop.repository.UserRepository;
import dataart.trainee.internetshop.service.impl.UserDetailsServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;

import java.text.ParseException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserDatailsServiceTest {
    UserRepository userRepository = Mockito.mock(UserRepository.class);

    UserDetailsServiceImpl userDetailsService;
    String email = "1@gmail.com";
    @BeforeEach
    void init() throws ParseException {
        userDetailsService = new UserDetailsServiceImpl(userRepository);
        Mockito.when(userRepository.findByEmail(email)).thenReturn(DefaultUser.getDefaultUser());
    }
    @Test
    void loadUserByUsernameTest() {
        Assertions.assertEquals(userDetailsService.loadUserByUsername(email).getUsername(), email);
    }

}
