package dataart.trainee.internetshop;

import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.models.product.Product;
import dataart.trainee.internetshop.models.product.Type;
import dataart.trainee.internetshop.repository.TypeRepository;
import dataart.trainee.internetshop.service.mappers.ProductMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;

import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductMapperTest {

    TypeRepository typeRepository = Mockito.mock(TypeRepository.class);
    private final ProductMapper productMapper = new ProductMapper(typeRepository);

    @BeforeAll
    void init() {
        Type type = Type.builder()
                .name("Car")
                .build();
        Mockito.when(typeRepository.findByName("Car")).thenReturn(type);
    }

    @Test
    public void productDataToProduct() {
        ArrayList<String> types = new ArrayList<>();
        ArrayList<String> types1 = new ArrayList<>();
        types.add("Car");
        ProductData productData = ProductData.builder()
                .name("Car1")
                .description("No")
                .amount(12)
                .price(12.4)
                .types(types)
                .build();
        Product product = productMapper.productDataToProduct(productData);
        ProductData productData1 = ProductData.builder()
                .name("Car1")
                .description("No")
                .amount(12)
                .price(12.4)
                .types(types1)
                .build();
        Product product1 = productMapper.productDataToProduct(productData1);
        Assertions.assertTrue(compareProductAndProductData(product, productData));
        Assertions.assertTrue(compareProductAndProductData(product1, productData1));
    }

    @Test
    public void productToProductDataTest() {
        int amount = 5;
        String name = "name";
        double price = 100.0;
        String description = "None";

        Product product = Product.builder()
                .amount(amount)
                .name(name)
                .price(price)
                .description(description)
                .id(Long.parseLong("1"))
                .build();
        ProductData productData = productMapper.productToProductData(product);
        Assertions.assertTrue(compareProductAndProductData(product, productData));
        Assertions.assertEquals(product.getId(), productData.getId());
    }

    @Test
    public void productDataToProductTest() {
        int amount = 5;
        String name = "name";
        double price = 100.0;
        String description = "None";

        ProductData productData = ProductData.builder()
                .amount(amount)
                .name(name)
                .price(price)
                .description(description)
                .build();
        Product product = productMapper.productDataToProduct(productData);
        Assertions.assertTrue(compareProductAndProductData(product, productData));
    }

    private boolean compareProductAndProductData(Product product, ProductData productData) {
        return (product.getAmount().equals(productData.getAmount()))
                && (product.getDescription().equals(productData.getDescription()))
                && (product.getName().equals(productData.getName()))
                && (product.getPrice().equals(productData.getPrice()));
    }
}
