package dataart.trainee.internetshop.models.enums;

public enum UserRole {
    ADMIN,
    USER
}
