package dataart.trainee.internetshop.models.product;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "products")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    @NotNull
    private String name;

    private String description;

    @NotNull
    @Min(0)
    private Integer amount;

    @NotNull
    private Double price;

    @ManyToMany
    @JoinTable(name="PRODUCT_TYPES",
            joinColumns =@JoinColumn(name="PRODUCT_ID", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name = "TYPE_ID", referencedColumnName="id"))
    private Set<Type> types = new HashSet<>();

}
