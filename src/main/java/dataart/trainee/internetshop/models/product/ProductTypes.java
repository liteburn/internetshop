package dataart.trainee.internetshop.models.product;


import lombok.*;

import javax.persistence.*;

@Entity(name = "product_types")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductTypes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "type_id")
    private Long typeId;
}
