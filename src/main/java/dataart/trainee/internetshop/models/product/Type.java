package dataart.trainee.internetshop.models.product;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;


@Entity(name = "type")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty(message = "Type could not be empty")
    @Column(unique = true)
    private String name;
}
