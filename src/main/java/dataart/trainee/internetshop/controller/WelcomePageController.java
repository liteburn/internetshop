package dataart.trainee.internetshop.controller;

import dataart.trainee.internetshop.constants.Routes;
import dataart.trainee.internetshop.service.UserService;
import dataart.trainee.internetshop.service.exceptions.NoSuchCredentialsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class WelcomePageController {
    @Autowired
    private UserService userService;

    @GetMapping
    public String starting(Principal principal, final Model model) throws NoSuchCredentialsException {
        if (principal == null) {
            model.addAttribute("user", null);
        } else {
            model.addAttribute("user", userService.getUser(principal.getName()));
        }
        return Routes.WELCOME_PATH;
    }

    @GetMapping("/error")
    public String error() {
        return Routes.ERROR_PATH;
    }
}
