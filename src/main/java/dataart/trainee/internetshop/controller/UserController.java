package dataart.trainee.internetshop.controller;

import dataart.trainee.internetshop.constants.Routes;
import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.models.enums.UserRole;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.NoSuchCredentialsException;
import dataart.trainee.internetshop.service.exceptions.UserAlreadyExistException;
import dataart.trainee.internetshop.service.exceptions.WrongPasswordException;
import dataart.trainee.internetshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserData user(Principal principal) throws NoSuchCredentialsException {
        if (principal == null) {
            return null;
        } else {
            return userService.getUser(principal.getName());
        }
    }

    @GetMapping("/register")
    public String register(final Model model) {
        model.addAttribute("userData", new UserData());
        return Routes.REGISTER_PATH;
    }

    @PostMapping("/register")
    public String userRegistration(final @Valid UserData userData, final BindingResult bindingResult, final Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("registrationForm", userData);
            return Routes.REGISTER_PATH;
        }
        try {
            userService.register(userData);
        } catch (UserAlreadyExistException e) {
            bindingResult.rejectValue("email", "userData.email", e.getMessage());
            model.addAttribute("registrationForm", userData);
            return Routes.REGISTER_PATH;
        } catch (WrongPasswordException e) {
            bindingResult.rejectValue("password", "userData.password", e.getMessage());
            model.addAttribute("registrationForm", userData);
            return Routes.REGISTER_PATH;
        }
        return "redirect:" + Routes.LOGIN_URL;
    }

    @GetMapping("/users")
    public String getUsers(final Model model, @RequestParam(required = false) OrderEnum order) {
        model.addAttribute("users", userService.getAllUsers(order));
        return Routes.USERS_PATH;
    }

    @GetMapping("/user/{id}")
    public String getUser(final Model model, @PathVariable long id) throws NoSuchCredentialsException {
        model.addAttribute("userData", userService.getUser(id));
        model.addAttribute("role", UserRole.USER);
        return Routes.USER_PATH;
    }

    @PutMapping("/user/{id}")
    public String updateUser(@PathVariable long id, UserData userData) {
        userService.updateUser(userData);
        return "redirect:" + Routes.USER_URL + id;
    }

    @DeleteMapping("/user/{id}/delete")
    public String deleteUser(@PathVariable long id) {
        userService.deleteUser(id);
        return "redirect:" + Routes.DEFAULT_URL;
    }

}
