package dataart.trainee.internetshop.controller.product;

import dataart.trainee.internetshop.constants.Attributes;
import dataart.trainee.internetshop.constants.Routes;
import dataart.trainee.internetshop.dto.product.TypeData;
import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.service.TypeService;
import dataart.trainee.internetshop.service.UserService;
import dataart.trainee.internetshop.service.exceptions.NoSuchCredentialsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/products")
@Slf4j
public class TypeController {

    @Autowired
    private TypeService typeService;
    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserData user(Principal principal) throws NoSuchCredentialsException {
        if (principal == null) {
            return null;
        } else {
            return userService.getUser(principal.getName());
        }
    }

    @GetMapping("addType")
    public String addType(final Model model) {
        model.addAttribute(Attributes.TYPE_DATA, new TypeData());
        return Routes.ADD_TYPE_PATH;
    }

    @PostMapping("addType")
    public String addType(final @Valid TypeData typeData, final BindingResult bindingResult, final Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(Attributes.TYPE_DATA, typeData);
            return Routes.ADD_TYPE_PATH;
        }
        typeService.addType(typeData);
        return Routes.ADD_TYPE_PATH;
    }
}
