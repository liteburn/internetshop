package dataart.trainee.internetshop.controller.product;

import dataart.trainee.internetshop.constants.Attributes;
import dataart.trainee.internetshop.constants.Routes;
import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.service.CartService;
import dataart.trainee.internetshop.service.ProductService;
import dataart.trainee.internetshop.service.TypeService;
import dataart.trainee.internetshop.service.UserService;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.InvalidAmountException;
import dataart.trainee.internetshop.service.exceptions.NoSuchCredentialsException;
import dataart.trainee.internetshop.service.exceptions.NoSuchProductException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;


@Controller
@RequestMapping("products")
@Slf4j
public class ProductController {
    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private CartService cartService;

    @ModelAttribute("user")
    public UserData user(Principal principal) throws NoSuchCredentialsException {
        if (principal == null) {
            return null;
        } else {
            return userService.getUser(principal.getName());
        }
    }

    @ModelAttribute("admin")
    public Boolean isAdmin() {
        return userService.hasAdminRole();
    }

    @GetMapping
    public String getAllProducts(final Model model, @RequestParam(required = false) String category, @RequestParam(required = false) String name, @RequestParam(required = false) OrderEnum order) {
        model.addAttribute("products", productService.getProducts(name, category, order));
        return Routes.PRODUCTS_PATH;
    }


    @GetMapping("/{id}")
    public String getProductById(final Model model, @PathVariable int id) {
        try {
            ProductData product = productService.getProduct(id);
            model.addAttribute(Attributes.PRODUCT_DATA, product);
            model.addAttribute(Attributes.TYPE_DATA, typeService.getAllTypes());
            return Routes.PRODUCT_PATH;
        } catch (NoSuchProductException noSuchProductException) {
            return "redirect:" + Routes.PRODUCTS_URL;
        }

    }


    @GetMapping("/addProduct")
    public String addProduct(final Model model) {
        model.addAttribute(Attributes.PRODUCT_DATA, new ProductData());
        model.addAttribute(Attributes.TYPE_DATA, typeService.getAllTypes());
        return Routes.ADD_PRODUCT_PATH;
    }

    @PostMapping("/addProduct")
    public String addProduct(final @Valid ProductData productData, final BindingResult bindingResult, final Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(Attributes.PRODUCT_DATA, productData);
            model.addAttribute(Attributes.TYPE_DATA, typeService.getAllTypes());
            return Routes.ADD_PRODUCT_PATH;
        }
        productService.addProduct(productData);
        return "redirect:" + Routes.PRODUCTS_URL;
    }

    @PutMapping("/update")
    public String updateProduct(@ModelAttribute ProductData productData, @RequestParam long id) {
        productService.updateProduct(id, productData);
        return "redirect:" + Routes.PRODUCTS_URL + id;
    }

    @DeleteMapping("/delete")
    public String deleteProduct(@ModelAttribute ProductData product, @RequestParam Long id) {
        productService.deleteProduct(id);
        return "redirect:" + Routes.PRODUCTS_URL;
    }

    @PostMapping("/addProductToCart")
    public String addProductToCart(@RequestParam long id, @RequestParam int amount) {
        try {
            cartService.addProduct(id, amount);
        } catch (InvalidAmountException err) {
            return Routes.PRODUCT_PATH;
        }

        return "redirect:" + Routes.PRODUCTS_URL + id;
    }

    @PostMapping("/removeProductFromCart")
    public String removeProductFromCart(@RequestParam Long id) {

        cartService.removeProduct(id);
        return "redirect:" + Routes.CART_URL;
    }


    @GetMapping("/cart")
    public String getCart(final Model model) {
        List<ProductData> cartData = cartService.getProducts();
        model.addAttribute("cart", cartData);
        model.addAttribute("sum", productService.sumUpProducts(cartData));
        return Routes.CART_PATH;
    }

    @PostMapping("/buyCart")
    public String buyCart() {
        cartService.buyCart();
        return "redirect:" + Routes.CART_URL;
    }

    @PostMapping("/clearCart")
    public String clearCart() {
        cartService.clearCart();
        return "redirect:" + Routes.CART_URL;
    }
}
