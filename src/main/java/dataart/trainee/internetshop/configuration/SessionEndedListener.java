package dataart.trainee.internetshop.configuration;

import dataart.trainee.internetshop.models.User;
import dataart.trainee.internetshop.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SessionEndedListener implements ApplicationListener<SessionDestroyedEvent> {
    @Autowired
    private CartService cartService;

    @Override
    public void onApplicationEvent(SessionDestroyedEvent sessionDestroyedEvent) {
        for (SecurityContext securityContext : sessionDestroyedEvent.getSecurityContexts()) {
            Authentication authentication = securityContext.getAuthentication();
            if (authentication.getPrincipal() == null){
                return;
            }
            User user = (User) authentication.getPrincipal();

            log.info("Clear cart of user with email: " + user.getEmail());
            cartService.clearCart();
        }
    }
}
