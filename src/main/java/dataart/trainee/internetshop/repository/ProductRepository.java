package dataart.trainee.internetshop.repository;

import dataart.trainee.internetshop.models.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("SELECT product FROM products product ORDER BY product.name")
    List<Product> findAll();

    List<Product> findAllByOrderByNameAsc();

    List<Product> findAllByOrderByNameDesc();

    List<Product> findAllByNameOrderByNameAsc(String name);

    List<Product> findAllByNameOrderByNameDesc(String name);

    Product findById(long id);

}
