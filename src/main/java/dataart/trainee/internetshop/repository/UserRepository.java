package dataart.trainee.internetshop.repository;

import dataart.trainee.internetshop.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    User findById(long id);

    @Query("SELECT u FROM Users u")
    List<User> findAll();

    List<User> findAllByOrderByNameAsc();

    List<User> findAllByOrderByNameDesc();

    List<User> findAllByOrderByDateAsc();

    List<User> findAllByOrderByDateDesc();

    List<User> findAllByOrderByEmailAsc();

    List<User> findAllByOrderByEmailDesc();
}
