package dataart.trainee.internetshop.repository;

import dataart.trainee.internetshop.models.product.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Long> {
    List<Cart> findAllByUserId(Long userId);

    Cart findByUserIdAndProductId(Long userId, Long productId);

}
