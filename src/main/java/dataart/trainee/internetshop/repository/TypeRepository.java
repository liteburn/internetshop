package dataart.trainee.internetshop.repository;

import dataart.trainee.internetshop.models.product.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TypeRepository extends JpaRepository<Type, Long> {
    @Query("SELECT t FROM type t")
    List<Type> findAll();

    Type findByName(String name);
}
