package dataart.trainee.internetshop.service;

import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.NoSuchProductException;

import java.util.List;

public interface ProductService {
    ProductData addProduct(final ProductData productData);

    List<ProductData> getProducts(final String name, final String category, OrderEnum order);

    ProductData getProduct(final long id) throws NoSuchProductException;

    void deleteProduct(Long id);

    double sumUpProducts(List<ProductData> cartData);

    ProductData updateProduct(long productId, ProductData productData);
}
