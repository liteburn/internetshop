package dataart.trainee.internetshop.service;

import dataart.trainee.internetshop.dto.product.TypeData;

import java.util.List;

public interface TypeService {
    List<TypeData> getAllTypes();

    TypeData addType(final TypeData typeData);
}
