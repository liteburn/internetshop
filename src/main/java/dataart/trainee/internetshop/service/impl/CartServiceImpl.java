package dataart.trainee.internetshop.service.impl;

import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.models.User;
import dataart.trainee.internetshop.models.product.Cart;
import dataart.trainee.internetshop.models.product.Product;
import dataart.trainee.internetshop.repository.CartRepository;
import dataart.trainee.internetshop.repository.ProductRepository;
import dataart.trainee.internetshop.service.CartService;
import dataart.trainee.internetshop.service.exceptions.InvalidAmountException;
import dataart.trainee.internetshop.service.mappers.ProductMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackFor = SQLException.class)
@AllArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductMapper productMapper;

    @Override
    public ProductData addProduct(long productId, int amount) throws InvalidAmountException {
        log.info("Product with id - " + productId + " added to user with id - " + getUserId());
        updateProductWithSubtractedAmount(productId, amount);
        cartRepository.save(updatedCart(getUserId(), productId, amount));
        return null;
    }


    @Override
    public void removeProduct(long productId) {
        Cart cartElement = cartRepository.findByUserIdAndProductId(getUserId(), productId);
        if (cartElement == null){
            return;
        }
        removeProductFromCart(productId, cartElement.getAmount());
        cartRepository.delete(cartElement);
    }

    @Override
    public void clearCart() {
        clearCart(getUserId());
    }

    @Override
    public void clearCart(long userId) {
        for (Cart cartElement : cartRepository.findAllByUserId(userId)) {
            removeProductFromCart(cartElement.getProductId(), cartElement.getAmount());
            cartRepository.delete(cartElement);
        }
    }

    @Override
    public List<ProductData> getProducts() {
        List<ProductData> products = new ArrayList<>();
        for (Cart cartElement : cartRepository.findAllByUserId(getUserId())) {
            addProductToProductsDataCart(cartElement, products);
        }
        return products;
    }

    @Override
    public void buyCart() {
        log.info("User with id - " + getUserId() + " bought his cart.");
        cartRepository.deleteAll(cartRepository.findAllByUserId(getUserId()));
    }

    private void updateProductWithSubtractedAmount(long productId, int amount) throws InvalidAmountException {
        Product product = productRepository.findById(productId);
        validateAmount(product.getAmount(), amount);
        product.setAmount(product.getAmount() - amount);
    }

    private ProductData getProductWithSetAmount(long productId, int amount) {
        ProductData productData = productMapper.productToProductData(productRepository.findById(productId));
        productData.setAmount(amount);
        return productData;
    }

    private void removeProductFromCart(long productId, int amount) {
        Product product = productRepository.findById(productId);
        if (product == null) {
            return;
        }
        log.info("Product is back to products form user with id -" + getUserId());
        product.setAmount(product.getAmount() + amount);
    }

    private Cart updatedCart(long userId, long productId, int amount) {
        Cart cart = cartRepository.findByUserIdAndProductId(userId, productId);
        if (cart == null) {
            cart = Cart.builder()
                    .userId(userId)
                    .productId(productId)
                    .build();
        }
        cart.setAmount(cart.getAmount() + amount);
        return cart;
    }

    public long getUserId() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getId();
    }

    void validateAmount(int max, int amount) throws InvalidAmountException {
        if (amount > max) {
            throw new InvalidAmountException("Amount is greater then total amount of product.");
        }
        if (amount < 1) {
            throw new InvalidAmountException("Amount have to be positive number.");
        }
    }

    void addProductToProductsDataCart(Cart cartElement, List<ProductData> productsData) {
        long productId = cartElement.getProductId();
        if (!checkIfProductExists(productId)) {
            removeProduct(cartElement.getProductId());
        }
        productsData.add(getProductWithSetAmount(productId, cartElement.getAmount()));
    }

    boolean checkIfProductExists(long productId) {
        return productRepository.findById(productId) != null;
    }
}
