package dataart.trainee.internetshop.service.impl;

import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.models.product.Product;
import dataart.trainee.internetshop.models.product.Type;
import dataart.trainee.internetshop.repository.ProductRepository;
import dataart.trainee.internetshop.repository.TypeRepository;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.mappers.ProductMapper;
import dataart.trainee.internetshop.service.ProductService;
import dataart.trainee.internetshop.service.exceptions.NoSuchProductException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private TypeRepository typeRepository;

    @Override
    public ProductData addProduct(ProductData productData) {
        log.info("Product - " + productData.getName() + " added to products.");
        return productMapper.productToProductData(productRepository.save(productMapper.productDataToProduct(productData)));
    }


    @Override
    public List<ProductData> getProducts(String name, String category, OrderEnum order) {
        List<ProductData> productData = new ArrayList<>();
        for (Product product : getOrderedProducts(name, category, order)) {
            productData.add(productMapper.productToProductData(product));
        }

        return productData;
    }

    @Override
    public ProductData getProduct(long id) throws NoSuchProductException {
        if (!checkIfProductExist(id)) {
            throw new NoSuchProductException("No such product founded");
        }
        return productMapper.productToProductData(productRepository.findById(id));
    }

    @Override
    public void deleteProduct(Long id) {
        if (checkIfProductExist(id)) {
            productRepository.deleteById(id);
        }
    }

    @Override
    public double sumUpProducts(List<ProductData> cartData) {
        double sum = 0;
        for (ProductData product : cartData) {
            sum += product.getAmount() * product.getPrice();
        }
        return Precision.round(sum, 2);
    }

    @Override
    public ProductData updateProduct(long productId, ProductData productData) {
        Product product = productRepository.findById(productId);
        product = productMapper.updateProductWithProductData(product, productData);
        productRepository.save(product);
        return productData;
    }

    private boolean checkIfProductExist(long id) {
        return productRepository.findById(id) != null;
    }

    private List<Product> getOrderedProducts(String name, String category, OrderEnum order) {
        if (order == null) {
            order = OrderEnum.NAME_ASC;
        }
        List<Product> products;
        if (name == null) {
            products = getOrderedProductsWithoutName(order);
        } else {
            products = getOrderedProductsWithName(order, name);
        }
        if (typeRepository.findByName(category) != null) {
            Type type = typeRepository.findByName(category);
            return products.stream()
                    .filter(i -> i.getTypes().contains(type))
                    .toList();
        }
        return products;
    }

    private List<Product> getOrderedProductsWithoutName(OrderEnum order) {
        return switch (order) {
            case NAME_ASC -> productRepository.findAllByOrderByNameAsc();
            default -> productRepository.findAllByOrderByNameDesc();
        };
    }

    private List<Product> getOrderedProductsWithName(OrderEnum order, String name) {
        return switch (order) {
            case NAME_ASC -> productRepository.findAllByNameOrderByNameAsc(name);
            default -> productRepository.findAllByNameOrderByNameDesc(name);
        };
    }
}
