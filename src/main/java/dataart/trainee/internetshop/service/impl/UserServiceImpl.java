package dataart.trainee.internetshop.service.impl;

import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.models.enums.UserRole;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.NoSuchCredentialsException;
import dataart.trainee.internetshop.service.exceptions.UserAlreadyExistException;
import dataart.trainee.internetshop.service.exceptions.WrongPasswordException;
import dataart.trainee.internetshop.models.User;
import dataart.trainee.internetshop.repository.UserRepository;
import dataart.trainee.internetshop.service.UserService;
import dataart.trainee.internetshop.service.mappers.UserMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    UserMapper userMapper;

    @Override
    public UserData register(UserData user) throws UserAlreadyExistException, WrongPasswordException {

        if (checkIfUserExist(user.getEmail())) {
            throw new UserAlreadyExistException("User already exists for this email");
        } else if (!user.getPassword().equals(user.getRepeatPassword())) {
            throw new WrongPasswordException("Password didn't match");
        }
        User userEntity = userMapper.userDataToUser(user);
        userRepository.save(userEntity);
        log.info("Registered user with email:" + userEntity.getEmail());
        return userMapper.userToUserData(userEntity);
    }


    @Override
    public boolean checkIfUserExist(String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    public boolean hasAdminRole() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
    }

    @Override
    public void deleteUser(long id) {
        User user = userRepository.findById(id);
        if (user != null && user.getUserRole() != UserRole.ADMIN) {
            log.info("Deleted user with id: " + id);
            userRepository.deleteById(id);
        }
    }

    @Override
    public UserData updateUser(UserData userData) {
        if (userRepository.findByEmail(userData.getEmail()) == null) {
            // Have to throw error
            return new UserData();
        }
        log.info("Updated user with email:" + userData.getEmail());
        userRepository.save(userMapper.updateUserWithUserData(userRepository.findByEmail(userData.getEmail()), userData));
        return userData;
    }


    @Override
    public UserData getUser(String email) throws NoSuchCredentialsException {
            if(userRepository.findByEmail(email) == null){
                throw new NoSuchCredentialsException("User with this email does not exhist");
            }
            return userMapper.userToUserData(userRepository.findByEmail(email));
    }

    @Override
    public UserData getUser(long id) throws NoSuchCredentialsException {
        if(userRepository.findById(id) == null){
            throw new NoSuchCredentialsException("User with this email does not exhist");
        }
        return userMapper.userToUserData(userRepository.findById(id));
    }

    public List<UserData> getAllUsers(OrderEnum order) {
        return userMapper.usersToUsersData(getOrderedUsers(order));
    }

    public List<User> getOrderedUsers(OrderEnum order) {
        if (order == null) {
            order = OrderEnum.NAME_ASC;
        }
        return switch (order) {
            case NAME_ASC -> userRepository.findAllByOrderByNameAsc();
            case NAME_DESC -> userRepository.findAllByOrderByNameDesc();
            case DATE_ASC -> userRepository.findAllByOrderByDateAsc();
            case EMAIL_ASC -> userRepository.findAllByOrderByEmailAsc();
            case EMAIL_DESC -> userRepository.findAllByOrderByEmailDesc();
            default -> userRepository.findAllByOrderByDateDesc();
        };
    }
}
