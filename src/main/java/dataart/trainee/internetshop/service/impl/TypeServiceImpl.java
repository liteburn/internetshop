package dataart.trainee.internetshop.service.impl;

import dataart.trainee.internetshop.dto.product.TypeData;
import dataart.trainee.internetshop.models.product.Type;
import dataart.trainee.internetshop.repository.TypeRepository;
import dataart.trainee.internetshop.service.TypeService;
import dataart.trainee.internetshop.service.mappers.TypeMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeRepository typeRepository;
    @Autowired
    private TypeMapper typeMapper;

    @Override
    public List<TypeData> getAllTypes() {
        List<TypeData> typeData = new ArrayList<>();
        for (Type type : typeRepository.findAll()) {
            typeData.add(typeMapper.typeToTypeData(type));
        }
        return typeData;
    }

    @Override
    public TypeData addType(TypeData typeData) {
        log.info("Type - " + typeData.getName() + " added to types");
        typeRepository.save(typeMapper.typeDataToType(typeData));
        return typeData;
    }
}
