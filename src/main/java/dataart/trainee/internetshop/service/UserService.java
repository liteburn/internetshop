package dataart.trainee.internetshop.service;

import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.NoSuchCredentialsException;
import dataart.trainee.internetshop.service.exceptions.UserAlreadyExistException;
import dataart.trainee.internetshop.service.exceptions.WrongPasswordException;

import java.util.List;

public interface UserService {

    UserData register(final UserData user) throws UserAlreadyExistException, WrongPasswordException;

    boolean checkIfUserExist(final String email);

    boolean hasAdminRole();

    List<UserData> getAllUsers(OrderEnum order);

    UserData getUser(String email) throws NoSuchCredentialsException;

    UserData getUser(long id) throws NoSuchCredentialsException;

    void deleteUser(long id);

    UserData updateUser(UserData userData);
}
