package dataart.trainee.internetshop.service.exceptions;

public class NoSuchCredentialsException extends Exception{
    public NoSuchCredentialsException() {
        super();
    }


    public NoSuchCredentialsException(String message) {
        super(message);
    }


    public NoSuchCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }
}
