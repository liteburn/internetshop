package dataart.trainee.internetshop.service.exceptions;

public class InvalidAmountException extends Exception{
    public InvalidAmountException(String message){
        super(message);
    }
}
