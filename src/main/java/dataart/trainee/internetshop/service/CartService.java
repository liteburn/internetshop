package dataart.trainee.internetshop.service;

import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.service.exceptions.InvalidAmountException;

import java.util.List;

public interface CartService {
    ProductData addProduct(long productId, int amount) throws InvalidAmountException;

    void removeProduct(long productId);

    void clearCart();

    void clearCart(long userId);

    List<ProductData> getProducts();

    void buyCart();

    long getUserId();
}
