package dataart.trainee.internetshop.service.mappers;

import dataart.trainee.internetshop.dto.product.TypeData;
import dataart.trainee.internetshop.models.product.Type;
import org.springframework.stereotype.Component;

@Component
public class TypeMapper {
    public TypeData typeToTypeData(Type type) {
        return TypeData.builder()
                .name(type.getName())
                .id(type.getId())
                .build();
    }

    public Type typeDataToType(TypeData typeData) {
        return Type.builder()
                .name(typeData.getName())
                .build();
    }
}
