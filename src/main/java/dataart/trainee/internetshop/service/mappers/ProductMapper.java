package dataart.trainee.internetshop.service.mappers;

import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.models.product.Product;
import dataart.trainee.internetshop.models.product.Type;
import dataart.trainee.internetshop.repository.TypeRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class ProductMapper {

    @Autowired
    private TypeRepository typeRepository;

    public Product productDataToProduct(ProductData productData) {
        Product product = Product.builder()
                .amount(productData.getAmount())
                .name(productData.getName())
                .price(productData.getPrice())
                .description(productData.getDescription())
                .build();
        Set<Type> types = new HashSet<>();
        if (productData.getTypes() != null) {
            for (String name : productData.getTypes()) {
                types.add(typeRepository.findByName(name));
            }
        }
        product.setTypes(types);
        return product;
    }

    public ProductData productToProductData(Product product) {
        ProductData productData = ProductData.builder()
                .amount(product.getAmount())
                .name(product.getName())
                .price(product.getPrice())
                .description(product.getDescription())
                .id(product.getId())
                .build();
        List<String> types = new ArrayList<>();
        if (product.getTypes() != null) {
            for (Type type : product.getTypes()) {
                types.add(type.getName());
            }
        }
        productData.setTypes(types);
        return productData;
    }

    public Product updateProductWithProductData(Product product, ProductData productData){
        if (productData.getTypes() != null) {
            Set<Type> types = new HashSet<>();
            for (String name : productData.getTypes()) {
                types.add(typeRepository.findByName(name));
            }
            product.setTypes(types);
        }
        product.setAmount(productData.getAmount());
        product.setDescription(productData.getDescription());
        product.setPrice(productData.getPrice());
        return product;
    }

}
