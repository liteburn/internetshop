package dataart.trainee.internetshop.service.mappers;

import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.models.User;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class UserMapper {

    @Autowired
    PasswordEncoder passwordEncoder;

    public List<UserData> usersToUsersData(List<User> users) {
        List<UserData> usersData = new ArrayList<>();
        for (User user : users) {
            usersData.add(userToUserData(user));
        }
        return usersData;
    }

    public UserData userToUserData(User user) {
        return UserData.builder()
                .name(user.getName())
                .date(user.getDate())
                .email(user.getEmail())
                .surname(user.getSurname())
                .id(user.getId())
                .role(user.getUserRole())
                .build();
    }

    public User updateUserWithUserData(User user, UserData userData) {
        if (userData.getRole() != null) {
            user.setUserRole(userData.getRole());
        }
        if (userData.getName() != null) {
            user.setName(userData.getName());
        }
        return user;
    }

    public User userDataToUser(UserData userData) {
        return User.builder()
                .name(userData.getName())
                .surname(userData.getSurname())
                .email(userData.getEmail())
                .date(userData.getDate())
                .password(passwordEncoder.encode(userData.getPassword()))
                .build();
    }
}
