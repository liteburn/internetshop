package dataart.trainee.internetshop.service.enums;

public enum OrderEnum {
    NAME_ASC,
    NAME_DESC,
    DATE_ASC,
    DATE_DESC,
    EMAIL_ASC,
    EMAIL_DESC
}
