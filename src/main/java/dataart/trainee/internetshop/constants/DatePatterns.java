package dataart.trainee.internetshop.constants;

public final class DatePatterns {
    public static final String DEFAULT_DATE_PATTERN = "yyyy-mm-dd";

    private DatePatterns() {
    }
}
