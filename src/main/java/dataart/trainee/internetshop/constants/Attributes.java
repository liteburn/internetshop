package dataart.trainee.internetshop.constants;

public class Attributes {

    public static final String PRODUCT_DATA = "productData";

    public static final String TYPE_DATA = "typeData";

    private Attributes(){
    }
}
