package dataart.trainee.internetshop.constants;

public class Routes {
    public static final String DEFAULT_URL = "/";

    public static final String DEFAULT_AUTHENTICATED_URL = "/**";

    public static final String USERS_PATH = "user/users";

    public static final String USER_PATH = "user/user";

    public static final String USER_URL = "user/";

    public static final String LOGIN_URL = "login/";

    public static final String REGISTER_PATH = "account/register";

    public static final String REGISTER_URL = "/register";

    public static final String ERROR_PATH = "error";

    public static final String WELCOME_PATH = "welcome";

    public static final String PRODUCTS_PATH = "product/products";

    public static final String PRODUCTS_URL = "/products/";

    public static final String ADD_TYPE_PATH = "product/admin/addType";

    public static final String ADD_TYPE_URL = PRODUCTS_URL + "addType";

    public static final String PRODUCT_PATH = "product/product";

    public static final String ADD_PRODUCT_PATH = "product/admin/addProduct";

    public static final String ADD_PRODUCT_URL = PRODUCTS_URL + "addProduct";

    public static final String CART_PATH = "product/cart";

    public static final String CART_URL = PRODUCTS_URL + "cart/";

    public static final String STYLES_URL = "/style/**";

    public static final String DELETE_USER_URL = "/user/**/delete";

    private Routes() {
    }
}

