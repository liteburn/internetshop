package dataart.trainee.internetshop.constants;

import dataart.trainee.internetshop.models.User;
import dataart.trainee.internetshop.models.enums.UserRole;
import dataart.trainee.internetshop.security.Password;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class DefaultUser {
    private static final PasswordEncoder passwordEncoder = new Password().passwordEncoder();

    public static final String DEFAULT_EMAIL = "1@gmail.com";

    public static final String DEFAULT_PASSWORD = "password";

    public static final String DEFAULT_NAME = "admin";

    public static final String DEFAULT_SURNAME = "first";

    public static final String DEFAULT_DATE = "2000-11-20";

    public static User getDefaultUser() throws ParseException {
        return User
                .builder()
                .userRole(UserRole.ADMIN)
                .password(passwordEncoder.encode(DEFAULT_PASSWORD))
                .email(DEFAULT_EMAIL)
                .date(new SimpleDateFormat(DatePatterns.DEFAULT_DATE_PATTERN).parse(DEFAULT_DATE))
                .name(DEFAULT_NAME)
                .surname(DEFAULT_SURNAME)
                .build();
    }

    private DefaultUser() {
    }
}
