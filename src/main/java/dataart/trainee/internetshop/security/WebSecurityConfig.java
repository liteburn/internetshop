package dataart.trainee.internetshop.security;

import dataart.trainee.internetshop.constants.DefaultUser;
import dataart.trainee.internetshop.constants.Routes;
import dataart.trainee.internetshop.models.User;
import dataart.trainee.internetshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
        User user = DefaultUser.getDefaultUser();
        if (userRepository.findByEmail(DefaultUser.DEFAULT_EMAIL) == null) {
            userRepository.save(user);
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(Routes.REGISTER_URL, Routes.DEFAULT_URL, Routes.STYLES_URL).permitAll()
                .antMatchers(Routes.ADD_TYPE_URL, Routes.ADD_PRODUCT_URL, Routes.DELETE_USER_URL).hasRole("ADMIN")
                .antMatchers(Routes.DEFAULT_AUTHENTICATED_URL).authenticated()
                .and()
                .formLogin()
                .permitAll()
                .and()
                .logout()
                .permitAll();
        http.cors().and().csrf().disable();
    }


}