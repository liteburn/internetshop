package dataart.trainee.internetshop.restContoller.product;

import dataart.trainee.internetshop.dto.product.TypeData;
import dataart.trainee.internetshop.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/products/types")
public class TypeRestController {

    @Autowired
    private TypeService typeService;


    @GetMapping
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public List<TypeData> getTypes(){
        return typeService.getAllTypes();
    }

    @PostMapping
    @Secured({"ROLE_ADMIN"})
    public TypeData addType(final @RequestBody TypeData typeData) {
        return typeService.addType(typeData);
    }
}
