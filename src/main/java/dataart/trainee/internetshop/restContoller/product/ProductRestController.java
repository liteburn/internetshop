package dataart.trainee.internetshop.restContoller.product;

import dataart.trainee.internetshop.dto.product.ProductData;
import dataart.trainee.internetshop.service.CartService;
import dataart.trainee.internetshop.service.ProductService;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.InvalidAmountException;
import dataart.trainee.internetshop.service.exceptions.NoSuchProductException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/products")
public class ProductRestController {
    @Autowired
    private ProductService productService;
    @Autowired
    private CartService cartService;

    @GetMapping
    public List<ProductData> getProducts(@RequestParam(required = false) String category, @RequestParam(required = false) String name, @RequestParam(required = false) OrderEnum order) {
        return productService.getProducts(name, category, order);
    }

    @GetMapping("/{id}")
    public ProductData getProduct(@PathVariable int id) throws NoSuchProductException {
        return productService.getProduct(id);
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public ProductData addProduct(final @RequestBody @Valid ProductData productData) {
        return productService.addProduct(productData);
    }

    @PutMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ProductData updateProduct(@RequestBody ProductData productData, @PathVariable long id) {
        return productService.updateProduct(id, productData);
    }

    @DeleteMapping
    @Secured("ROLE_ADMIN")
    public void deleteProduct(@RequestParam Long id) {
        productService.deleteProduct(id);
    }

    @PostMapping("/cart")
    public ProductData addProductToCart(@RequestParam long id, @RequestParam int amount) throws InvalidAmountException {
        return cartService.addProduct(id, amount);
    }

    @DeleteMapping("/cart")
    public void removeProductFromCart(@RequestParam Long id) {
        cartService.removeProduct(id);
    }

    @GetMapping("/cart")
    public List<ProductData> getCart() {
        return cartService.getProducts();
    }

    @PostMapping("/buyCart")
    public void buyCart() {
        cartService.buyCart();
    }

    @PostMapping("/clearCart")
    public void clearCart() {
        cartService.clearCart();
    }
}
