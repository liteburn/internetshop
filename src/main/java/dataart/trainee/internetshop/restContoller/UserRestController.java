package dataart.trainee.internetshop.restContoller;


import dataart.trainee.internetshop.dto.user.UserData;
import dataart.trainee.internetshop.service.UserService;
import dataart.trainee.internetshop.service.enums.OrderEnum;
import dataart.trainee.internetshop.service.exceptions.NoSuchCredentialsException;
import dataart.trainee.internetshop.service.exceptions.UserAlreadyExistException;
import dataart.trainee.internetshop.service.exceptions.WrongPasswordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public List<UserData> getUsers(@RequestParam(required = false) OrderEnum order) {
        return userService.getAllUsers(order);
    }

    @GetMapping("/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public UserData getUser(@PathVariable long id) throws NoSuchCredentialsException {

        return userService.getUser(id);
    }

    @PostMapping
    public UserData register(@RequestBody UserData userData) throws UserAlreadyExistException, WrongPasswordException {
        return userService.register(userData);
    }



    @PutMapping("/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public UserData updateUser(@RequestBody UserData userData){
        return userService.updateUser(userData);
    }

    @DeleteMapping("/{id}")
    @Secured({"ROLE_ADMIN"})
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }
}
