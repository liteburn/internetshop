package dataart.trainee.internetshop;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"dataart.trainee.internetshop"})
public class ShopApplication {

    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(ShopApplication.class);
        app.run(args);
    }

}
