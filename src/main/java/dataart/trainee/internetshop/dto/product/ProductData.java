package dataart.trainee.internetshop.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductData {
    @NotEmpty(message = "Name could not be empty.")
    private String name;

    private Long id;

    private String description;

    @Positive
    private Integer amount;

    @Positive
    private Double price;

    private List<String> types;
}
