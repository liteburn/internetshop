package dataart.trainee.internetshop.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TypeData {
    @NotEmpty(message = "Name could not be empty.")
    private String name;

    private Long id;
}
