package dataart.trainee.internetshop.dto.user;

import dataart.trainee.internetshop.constants.DatePatterns;
import dataart.trainee.internetshop.models.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserData implements Serializable {
    private Long id;

    @NotEmpty(message = "Name can not be empty")
    private String name;

    private String surname;

    @NotEmpty(message = "Email can not be empty")
    @Email(message = "Please provide a valid email id")
    private String email;

    @DateTimeFormat(pattern = DatePatterns.DEFAULT_DATE_PATTERN)
    private Date date;

    @NotEmpty(message = "Password can not be empty")
    private String password;

    private UserRole role;

    private String repeatPassword;
}